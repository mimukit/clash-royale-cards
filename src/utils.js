const RARITY = {
  Common: 0,
  Rare: 1,
  Epic: 2,
  Legendary: 4,
};

const REQUIRED_CARDS = {
  10: [800, 200, 20, 2],
  11: [1000, 400, 50, 4],
  12: [2000, 800, 100, 10],
  13: [5000, 1000, 200, 20],
};

const getRequiredCard = (desiredLevel, currentLevel, rarity, count) => {
  if (rarity === 'Legendary') {
    return 0;
  }

  const requiredCards = REQUIRED_CARDS[parseInt(desiredLevel)][RARITY[rarity]];

  if (desiredLevel <= currentLevel) {
    return 0;
  }

  if (desiredLevel > currentLevel + 1) {
    return requiredCards;
  }

  return requiredCards - parseInt(count);
};

export const processData = currentDeck => {
  const requiredCards = [
    currentDeck.map(card =>
      getRequiredCard(10, card.displayLevel, card.rarity, card.count),
    ),

    currentDeck.map(card =>
      getRequiredCard(11, card.displayLevel, card.rarity, card.count),
    ),

    currentDeck.map(card =>
      getRequiredCard(12, card.displayLevel, card.rarity, card.count),
    ),

    currentDeck.map(card =>
      getRequiredCard(13, card.displayLevel, card.rarity, card.count),
    ),
  ];

  const processedData = {
    requiredCards,
    numberOfReq: [[], [], [], []],
    numberOfDays: [[], [], [], []],
    totalDays: [0, 0, 0, 0],
    totalToMax: 0,
  };

  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 8; j++) {
      const count = processedData.requiredCards[i][j];
      if (count < 0) {
        processedData.requiredCards[i][j] = 0;
        processedData.requiredCards[i + 1][j] =
          processedData.requiredCards[i + 1][j] + count;
      }
    }
  }

  // Number of Requests & days

  for (let i = 0; i < 4; i++) {
    currentDeck.forEach((card, j) => {
      const numReq = getNumberOfReq(
        processedData.requiredCards[i][j],
        card.rarity,
      );

      processedData.numberOfReq[i][j] = numReq;

      processedData.numberOfDays[i][j] = getNumberOfDays(numReq, card.rarity);
    });
  }

  processedData.totalDays = processedData.numberOfDays.map(level =>
    level.reduce((a, b) => a + b, 0),
  );

  processedData.totalToMax = processedData.totalDays.reduce((c, d) => c + d, 0);

  return processedData;
};

export const getNumberOfReq = (count, rarity) => {
  switch (rarity) {
    case 'Common':
      return Math.ceil(parseInt(count) / 40);

    case 'Rare':
      return Math.ceil(parseInt(count) / 4);

    case 'Epic':
      return Math.ceil(parseInt(count) / 4);

    case 'Legendary':
      return 0;

    default:
      break;
  }
};

export const getNumberOfDays = (req, rarity) => {
  switch (rarity) {
    case 'Common':
      return Math.ceil(parseInt(req) / 3);

    case 'Rare':
      return Math.ceil(parseInt(req) / 3);

    case 'Epic':
      return Math.ceil(parseInt(req) * 7);

    case 'Legendary':
      return 0;

    default:
      break;
  }
};
