import axios from 'axios';
import React, { useEffect, useState } from 'react';
import './App.css';
import { processData } from './utils';

const apiUrlBase =
  'https://cors-anywhere.herokuapp.com/https://api-v3.royaleapi.com';

const getData = async tag =>
  await axios.get(`${apiUrlBase}/player/${tag}`, {
    headers: {
      Authorization: `Bearer ${process.env.REACT_APP_API_ACCESS_TOKEN}`,
    },
  });

const App = () => {
  const [playerTag, setPlayerTag] = useState('2R90CUGQR');

  const [playerData, setPlayerData] = useState();

  const [processedData, setProcessedData] = useState({});

  const loadPlayerData = async tag => {
    try {
      const data = await getData(tag);
      setPlayerData(data.data);
    } catch (error) {
      alert('Error: player not found. Recheck tag');
      console.error(error);
    }
  };

  const onTagSubmit = e => {
    e.preventDefault();

    let formattedPlayerTag = playerTag;

    if (playerTag.charAt(0) === '#') {
      formattedPlayerTag = playerTag.substr(1);
    }

    loadPlayerData(formattedPlayerTag);
  };

  useEffect(() => {
    loadPlayerData(playerTag);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    console.log(playerData);
    if (playerData && playerData.currentDeck) {
      setProcessedData(processData(playerData.currentDeck));
    }
  }, [playerData]);

  const [totalToMax, setTotalToMax] = useState(0);
  const [totalDays, setTotalDays] = useState([0, 0, 0, 0]);

  useEffect(() => {
    setTotalToMax(processedData.totalToMax);
  }, [playerData, processedData]);

  useEffect(() => {
    setTotalDays(processedData.totalDays);
  }, [playerData, processedData]);

  const renderDeck = (currentDeck, processedData) => {
    return currentDeck.map((card, i) => (
      <tr key={card.id}>
        <td>
          <img src={card.icon} alt={card.name} className="card-image" />
        </td>
        <td>{card.name}</td>
        <td>{`${card.displayLevel} - ${card.rarity[0]}`}</td>
        <td>{card.count}</td>

        <td>{processedData.requiredCards[0][i]}</td>
        <td>{processedData.requiredCards[1][i]}</td>
        <td>{processedData.requiredCards[2][i]}</td>
        <td>{processedData.requiredCards[3][i]}</td>

        <td>{processedData.numberOfReq[0][i]}</td>
        <td>{processedData.numberOfReq[1][i]}</td>
        <td>{processedData.numberOfReq[2][i]}</td>
        <td>{processedData.numberOfReq[3][i]}</td>

        <td>
          <b>{processedData.numberOfDays[0][i]}</b>
        </td>
        <td>
          <b>{processedData.numberOfDays[1][i]}</b>
        </td>
        <td>
          <b>{processedData.numberOfDays[2][i]}</b>
        </td>
        <td>
          <b>{processedData.numberOfDays[3][i]}</b>
        </td>

        <td>
          {processedData.numberOfDays[0][i] +
            processedData.numberOfDays[1][i] +
            processedData.numberOfDays[2][i] +
            processedData.numberOfDays[3][i]}
        </td>
      </tr>
    ));
  };

  return (
    <div className="App">
      <header className="App-header">
        <div className="player-tag-form">
          <form>
            <label htmlFor="tag" style={{ fontWeight: 'bold', marginRight: 5 }}>
              Player's Tag:
            </label>
            <input
              type="text"
              id="tag"
              name="tag"
              value={playerTag}
              onChange={e => setPlayerTag(e.target.value.toUpperCase())}
            />
            <button style={{ marginLeft: 5 }} onClick={onTagSubmit}>
              Submit
            </button>
          </form>
        </div>

        {playerData && totalToMax >= 0 && totalDays ? (
          <>
            <h3>{`Player: ${playerData.name}`}</h3>
            <table style={{ width: 400 }}>
              <thead>
                <tr>
                  <th>Level</th>
                  <th>Days</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>10</td>
                  <td>{totalDays[0]}</td>
                </tr>
                <tr>
                  <td>11</td>
                  <td>{totalDays[1]}</td>
                </tr>
                <tr>
                  <td>12</td>
                  <td>{totalDays[2]}</td>
                </tr>
                <tr>
                  <td>13</td>
                  <td>{totalDays[3]}</td>
                </tr>
              </tbody>
            </table>
            <h3>
              Total to max: {totalToMax} day(s) ={' '}
              {(totalToMax / 365).toFixed(2)} year(s)
            </h3>
            <p style={{ color: '#FF6974' }}>
              <i>Legendary cards are ignored as those can not be requested</i>
            </p>

            <table>
              <thead>
                <tr>
                  <th rowSpan="2">Image</th>
                  <th rowSpan="2">Name</th>
                  <th rowSpan="2">C/L</th>
                  <th rowSpan="2">Count</th>
                  <th colSpan="4">Required</th>
                  <th colSpan="4">No of Req</th>
                  <th colSpan="4">Days</th>
                  <th rowSpan="2">Total</th>
                </tr>
                <tr>
                  <th>10</th>
                  <th>11</th>
                  <th>12</th>
                  <th>13</th>

                  <th>10</th>
                  <th>11</th>
                  <th>12</th>
                  <th>13</th>

                  <th>10</th>
                  <th>11</th>
                  <th>12</th>
                  <th>13</th>
                </tr>
              </thead>
              <tbody>{renderDeck(playerData.currentDeck, processedData)}</tbody>
            </table>
          </>
        ) : (
          <p>loading...</p>
        )}
      </header>
    </div>
  );
};

export default App;
