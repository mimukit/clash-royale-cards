const RARITY = {
  Common: 0,
  Rare: 1,
  Epic: 2,
  Legendary: 4,
};

const REQUIRED_CARDS = {
  11: [1000, 400, 50, 4],
  12: [2000, 800, 100, 10],
  13: [5000, 1000, 200, 20],
};

const getRequiredCard = (desiredLevel, currentLevel, rarity, count) => {
  if (desiredLevel < currentLevel) {
    return 0;
  }

  if (desiredLevel > currentLevel) {
    return REQUIRED_CARDS[parseInt(desiredLevel)][RARITY[rarity]];
  }
  return (
    REQUIRED_CARDS[parseInt(desiredLevel)][RARITY[rarity]] - parseInt(count)
  );
};

const res = getRequiredCard(11, '11', 'Common', '500');

const currentDeck = [
  {
    name: 'Hog Rider',
    id: 26000021,
    level: 10,
    maxLevel: 13,
    count: 516,
    rarity: 'Rare',
    requiredForUpgrade: 1000,
    leftToUpgrade: 484,
    displayLevel: 12,
    minLevel: 3,
    icon:
      'https://api-assets.clashroyale.com/cards/300/Ubu0oUl8tZkusnkZf8Xv9Vno5IO29Y-jbZ4fhoNJ5oc.png',
    key: 'hog-rider',
    elixir: 4,
    type: 'Troop',
    arena: 1,
    description:
      'Fast melee troop that targets buildings and can jump over the river. He followed the echoing call of "Hog Riderrrrr" all the way through the Arena doors.',
  },
  {
    name: 'Valkyrie',
    id: 26000011,
    level: 8,
    maxLevel: 13,
    count: 93,
    rarity: 'Rare',
    requiredForUpgrade: 400,
    leftToUpgrade: 307,
    displayLevel: 10,
    minLevel: 3,
    icon:
      'https://api-assets.clashroyale.com/cards/300/0lIoYf3Y_plFTzo95zZL93JVxpfb3MMgFDDhgSDGU9A.png',
    key: 'valkyrie',
    elixir: 4,
    type: 'Troop',
    arena: 2,
    description:
      'Tough melee fighter, deals area damage around her. Swarm or horde, no problem! She can take them all out with a few spins.',
  },
  {
    name: 'Archers',
    id: 26000001,
    level: 11,
    maxLevel: 13,
    count: 1263,
    rarity: 'Common',
    requiredForUpgrade: 2000,
    leftToUpgrade: 737,
    displayLevel: 11,
    minLevel: 1,
    icon:
      'https://api-assets.clashroyale.com/cards/300/W4Hmp8MTSdXANN8KdblbtHwtsbt0o749BbxNqmJYfA8.png',
    key: 'archers',
    elixir: 3,
    type: 'Troop',
    arena: 0,
    description:
      "A pair of lightly armored ranged attackers. They'll help you take down ground and air units, but you're on your own with hair coloring advice.",
  },
  {
    name: 'Bats',
    id: 26000049,
    level: 12,
    maxLevel: 13,
    count: 452,
    rarity: 'Common',
    requiredForUpgrade: 5000,
    leftToUpgrade: 4548,
    displayLevel: 12,
    minLevel: 1,
    icon:
      'https://api-assets.clashroyale.com/cards/300/EnIcvO21hxiNpoI-zO6MDjLmzwPbq8Z4JPo2OKoVUjU.png',
    key: 'bats',
    elixir: 2,
    type: 'Troop',
    arena: 5,
    description:
      'Spawns a handful of tiny flying creatures. Think of them as sweet, purple... balls of DESTRUCTION!',
  },
  {
    name: 'Inferno Tower',
    id: 27000003,
    level: 8,
    maxLevel: 13,
    count: 324,
    rarity: 'Rare',
    requiredForUpgrade: 400,
    leftToUpgrade: 76,
    displayLevel: 10,
    minLevel: 3,
    icon:
      'https://api-assets.clashroyale.com/cards/300/GSHY_wrooMMLET6bG_WJB8redtwx66c4i80ipi4gYOM.png',
    key: 'inferno-tower',
    elixir: 5,
    type: 'Building',
    arena: 4,
    description:
      'Defensive building, roasts targets for damage that increases over time. Burns through even the biggest and toughest enemies!',
  },
  {
    name: 'Earthquake',
    id: 28000014,
    level: 7,
    maxLevel: 13,
    count: 71,
    rarity: 'Rare',
    requiredForUpgrade: 200,
    leftToUpgrade: 129,
    displayLevel: 9,
    minLevel: 3,
    icon:
      'https://api-assets.clashroyale.com/cards/300/XeQXcrUu59C52DslyZVwCnbi4yamID-WxfVZLShgZmE.png',
    key: 'earthquake',
    elixir: 3,
    type: 'Spell',
    arena: 9,
    description:
      'Deals Damage per second to Troops and Crown Towers. Deals huge Building Damage! Does not affect flying units (it is an EARTHquake, after all).',
  },
  {
    name: 'Barbarian Barrel',
    id: 28000015,
    level: 4,
    maxLevel: 13,
    count: 57,
    rarity: 'Epic',
    requiredForUpgrade: 20,
    leftToUpgrade: -37,
    displayLevel: 9,
    minLevel: 6,
    icon:
      'https://api-assets.clashroyale.com/cards/300/Gb0G1yNy0i5cIGUHin8uoFWxqntNtRPhY_jeMXg7HnA.png',
    key: 'barbarian-barrel',
    elixir: 2,
    type: 'Spell',
    arena: 3,
    description:
      'It rolls over and damages anything in its path, then breaks open and out pops a Barbarian! How did he get inside?!',
  },
  {
    name: 'Giant Snowball',
    id: 28000017,
    level: 9,
    maxLevel: 13,
    count: 1144,
    rarity: 'Common',
    requiredForUpgrade: 800,
    leftToUpgrade: -344,
    displayLevel: 9,
    minLevel: 1,
    icon:
      'https://api-assets.clashroyale.com/cards/300/7MaJLa6hK9WN2_VIshuh5DIDfGwm0wEv98gXtAxLDPs.png',
    key: 'giant-snowball',
    elixir: 2,
    type: 'Spell',
    arena: 8,
    description:
      "It's HUGE! Once it began rolling down Frozen Peak, there was no stopping it. Enemies hit are knocked back and slowed down.",
  },
];

const requiredCards = [
  currentDeck.map(card =>
    getRequiredCard(11, card.displayLevel, card.rarity, card.count),
  ),

  currentDeck.map(card =>
    getRequiredCard(12, card.displayLevel, card.rarity, card.count),
  ),

  currentDeck.map(card =>
    getRequiredCard(13, card.displayLevel, card.rarity, card.count),
  ),
];

const processedData = requiredCards;

for (let i = 0; i < 2; i++) {
  for (let j = 0; j < 8; j++) {
    const count = processedData[i][j];
    if (count < 0) {
      processedData[i][j] = 0;
      processedData[i + 1][j] = processedData[i + 1][j] + count;
    }
  }
}
