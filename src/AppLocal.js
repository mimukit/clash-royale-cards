import React, { useEffect, useState } from 'react';
import './App.css';
import { playerDataDummy } from './data';
import { processData } from './utils';

const AppLocal = () => {
  const playerData = playerDataDummy;

  let processedData = processData(playerData.currentDeck);

  const [totalToMax, setTotalToMax] = useState(0);
  const [totalDays, setTotalDays] = useState([0, 0, 0, 0]);

  useEffect(() => {
    setTotalToMax(processedData.totalToMax);
  }, [playerData.currentDeck, processedData, processedData.totalToMax]);

  useEffect(() => {
    setTotalDays(processedData.totalDays);
  }, [playerData.currentDeck, processedData, processedData.totalDays]);

  const renderDeck = (currentDeck, processedData) => {
    return currentDeck.map((card, i) => (
      <tr key={card.id}>
        <td>
          <img src={card.icon} alt={card.name} className="card-image" />
        </td>
        <td>{card.name}</td>
        <td>{`${card.displayLevel} - ${card.rarity[0]}`}</td>
        <td>{card.count}</td>

        <td>{processedData.requiredCards[0][i]}</td>
        <td>{processedData.requiredCards[1][i]}</td>
        <td>{processedData.requiredCards[2][i]}</td>
        <td>{processedData.requiredCards[3][i]}</td>

        <td>{processedData.numberOfReq[0][i]}</td>
        <td>{processedData.numberOfReq[1][i]}</td>
        <td>{processedData.numberOfReq[2][i]}</td>
        <td>{processedData.numberOfReq[3][i]}</td>

        <td>{processedData.numberOfDays[0][i]}</td>
        <td>{processedData.numberOfDays[1][i]}</td>
        <td>{processedData.numberOfDays[2][i]}</td>
        <td>{processedData.numberOfDays[3][i]}</td>

        <td>
          {processedData.numberOfDays[0][i] +
            processedData.numberOfDays[1][i] +
            processedData.numberOfDays[2][i] +
            processedData.numberOfDays[3][i]}
        </td>
      </tr>
    ));
  };

  return (
    <div className="App">
      <header className="App-header">
        <div className="player-tag-form">
          <form>
            <label htmlFor="tag" style={{ fontWeight: 'bold' }}>
              Player's Tag:{' '}
            </label>
            <input type="text" id="tag" name="tag" />
            <input type="submit" />
          </form>
        </div>

        {playerData && processedData.totalToMax ? (
          <>
            <h3>{`${playerData.name}'s CR Deck`}</h3>
            <table style={{ width: 400 }}>
              <thead>
                <tr>
                  <th>Level</th>
                  <th>Days</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>10</td>
                  <td>{totalDays[0]}</td>
                </tr>
                <tr>
                  <td>11</td>
                  <td>{totalDays[1]}</td>
                </tr>
                <tr>
                  <td>12</td>
                  <td>{totalDays[2]}</td>
                </tr>
                <tr>
                  <td>13</td>
                  <td>{totalDays[3]}</td>
                </tr>
              </tbody>
            </table>

            <h3>
              Total to max: {totalToMax} day(s) ={' '}
              {(totalToMax / 365).toFixed(2)} year(s)
            </h3>

            <table>
              <thead>
                <tr>
                  <th rowSpan="2">Image</th>
                  <th rowSpan="2">Name</th>
                  <th rowSpan="2">C/L</th>
                  <th rowSpan="2">Count</th>
                  <th colSpan="4">Required</th>
                  <th colSpan="4">No of Req</th>
                  <th colSpan="4">Days</th>
                  <th rowSpan="2">Total</th>
                </tr>
                <tr>
                  <th>10</th>
                  <th>11</th>
                  <th>12</th>
                  <th>13</th>

                  <th>10</th>
                  <th>11</th>
                  <th>12</th>
                  <th>13</th>

                  <th>10</th>
                  <th>11</th>
                  <th>12</th>
                  <th>13</th>
                </tr>
              </thead>
              <tbody>{renderDeck(playerData.currentDeck, processedData)}</tbody>
            </table>
          </>
        ) : (
          <p>loading...</p>
        )}
      </header>
    </div>
  );
};

export default AppLocal;
